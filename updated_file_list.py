
import requests
import arrow
import urllib.parse
import json
import auth

token = auth.token
branch = "master"
base_path = "https://gitlab.com/api/v4/projects/"

def _prepare_tree_url(base_path, project_id):
    prepared_string = f"{base_path}{project_id}/repository/tree/"
    return prepared_string


def get_api_url(project_namespace):
    project_namespace_encoded = urllib.parse.quote(project_namespace).replace("/", "%2F")
    prepared_string = f"https://gitlab.com/api/v4/projects/{project_namespace_encoded}/"
    return prepared_string


def link_transformer(url):
    url = url.replace("sites/handbook/source/", "https://about.gitlab.com/")
    url = url.replace(".html.md", ".html")
    return url


def get_repo_file_record(project_namespace, file_path, token):
    file_path_encoded = urllib.parse.quote(file_path).replace("/", "%2F")
    prepared_url = get_api_url(project_namespace)+f"repository/files/{file_path_encoded}"
    response = requests.get(prepared_url, headers={'PRIVATE-TOKEN': token}, params={"ref":"master"})
    return response


def get_commit(project_namespace, commit_id, token):
    prepared_url = get_api_url(project_namespace)+f"repository/commits/{commit_id}"

    response = requests.get(prepared_url, headers={'PRIVATE-TOKEN': token})
    return response


def search_dir(tree_path, token, branch_name, responses = None):
    if responses == None:
        responses = []
    
    test_dir_path = "sites/handbook/source/handbook/support/workflows/"
    response = requests.get(
        tree_path,
        headers={'PRIVATE-TOKEN': token},
        params={"path": test_dir_path, "ref": branch_name }
    )
    tree_payload = json.loads(response.text)
    responses.append(tree_payload)
    try:
        while response.links['next'] != None:
            url = response.links['next']['url']
            response = requests.get(
                url,
                headers={'PRIVATE-TOKEN': token},
            )
            tree_payload = json.loads(response.text)
            responses.append(tree_payload)
    except KeyError:
        pass
    return responses

project_id = "gitlab-com/www-gitlab-com"
encoded_project_id = urllib.parse.quote(project_id).replace("/", "%2F")

print("\n\n\tGetting dir\n")

tree_path = _prepare_tree_url(base_path, encoded_project_id)
test_dir_path = "sites/handbook/source/handbook/support/workflows/"
response = requests.get(
    tree_path,
    headers={'PRIVATE-TOKEN': token},
    params={"path": test_dir_path, "ref": branch }
)

tree_path = _prepare_tree_url(base_path, encoded_project_id)
responses = search_dir(tree_path, token, branch)

sub_directories = []
files = []

for payload in responses:
    for item in payload:
        if item["type"] == "tree":
            sub_directories.append(item)
        if item["type"] == "blob":
            if "html.md" in item["path"]:
                files.append(item)


print(f"\n\n\tDirs: {len(sub_directories)}\n\tFiles: {len(files)}\n")

updated_files = []

namespace_unencoded = "gitlab-com/www-gitlab-com"

threshold_date = arrow.now().shift(days=-7)
print(f"Determining workflow updates since {threshold_date}...\n\n")

for file_result in files:
    file_obj_response = get_repo_file_record(namespace_unencoded,file_result["path"],token)
    file_obj = json.loads(file_obj_response.text)
    commit_id = file_obj["last_commit_id"]
    response = get_commit(namespace_unencoded, commit_id, token)
    commit_metadata = json.loads(response.text)
    commit_date = arrow.get(commit_metadata['committed_date'])
    if commit_date > threshold_date:
        file_obj["commit_date"] = commit_date
        updated_files.append(file_obj)

print("\n\n\nUPDATED FILES:")
for update in updated_files:
    file_name = update['file_name']
    date = update["commit_date"]
    path = update["file_path"]
    humanized = arrow.get(date).humanize()
    url = link_transformer(path)

    print(f"{humanized}\t-\t{file_name}\t-\t{url}")
    
